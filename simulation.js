require('./config/globals')

process.on('unhandledRejection', r => console.error(r))

const simulator = require('./simulator')

const coin = process.argv[2]
const increment = parseInt(process.argv[3])

try {
  simulator.start(coin, increment)
} catch (error) {
  setTimeout(() => {
    console.error(error)
  }, 1)
}
