const Datastore = require('nedb-promise')
const coins = require('../config/coins')
const indicators = require('../config/indicators')
const inceptionDates = require('../config/ico-dates')
const path = require('path')

const db = {}

class Database {
  constructor () {
    coins.forEach(coin => {
      Database._initializeCoin(coin)
    })

    indicators.forEach(indicator => {
      db[indicator] = Datastore({ filename: path.resolve(__dirname, 'indicators', indicator + '.txt'), autoload: true })
      db[indicator].ensureIndex({ fieldName: 'coin', unique: true })
    })
  }

  static _initializeCoin (coin) {
    const name = coin.toLowerCase()
    db[name] = Datastore({ filename: path.resolve(__dirname, 'coins', name + '.txt'), autoload: true })
    db[name].ensureIndex({ fieldName: 'time', unique: true })
  }

  _validateSymbol (coin) {
    coin = coin.toLowerCase()
    if (!db[coin]) {
      Database._initializeCoin(coin)
    }
    return coin
  }

  /**
   * { timestamp, dom }
   * @param coin
   * @param data
   * @returns {Promise<void>}
   */
  async saveDominance (coin, data) {
    coin = coin.toLowerCase()
    data = data.map(item => {
      let { timestamp, dom } = item
      if (isNaN(timestamp) || isNaN(dom)) {
        throw new Error('Invalid types for ' + timestamp + ' ' + dom)
      }
      timestamp = parseInt(timestamp)
      dom = parseFloat(dom)
      return { timestamp, dom }
    })
    const database = Datastore({ filename: path.resolve(__dirname, 'dominance', coin + '.txt'), autoload: true })
    await database.ensureIndex({ fieldName: 'timestamp', unique: true })
    await database.insert(data)
  }

  async saveObv (coin, data) {
    if (!data || !coin) throw new Error('Missing required arguments')
    coin = this._validateSymbol(coin)
    const { timestamp, obv, close } = data
    if (!timestamp || !obv || !close) throw new Error('Missing required properties')
    await db['obv'].update({ coin }, { $set: { timestamp, obv, close } }, { upsert: true })
  }

  async getIndicator (indicator, coin) {
    coin = this._validateSymbol(coin)
    indicator = indicator.toLowerCase()
    const doc = await db[indicator].cfindOne({ coin }).exec()
    return doc
  }

  /**
   * Inserts coin data to db.
   * Returns true if newly saved.
   * Returns false if data already exists
   * Throws error if w.e
   * @param coin
   * @param doc
   * @returns {Promise<boolean>}
   */
  async save (coin, doc) {
    coin = this._validateSymbol(coin)

    // Don't save if doc is before first price
    // if (this.getIsFirstPrice(coin, doc)) {
    //   return false
    // }

    doc.timestamp = doc.time * 1000

    // CryptoCompare returns negative time it means there are no prices
    if (doc.time < 0) {
      console.log('Doc time is less than 0', doc)
      return false
    }

    try {
      await db[coin].update({ time: doc.time }, doc, { upsert: true })
      return true
    } catch (error) {
      if (error.errorType === 'uniqueViolated') {
        return false
      }
      console.log('throwing becuase...?', error.key)
      throw error
    }
  }

  /**
   * Returns true if model price is before specified coin's first date
   * @param coin
   * @param doc
   */
  getIsFirstPrice (coin, doc) {
    coin = this._validateSymbol(coin)
    const priceMoment = moment(doc.time * 1000)
    const firstPriceMoment = moment(inceptionDates[coin])
    return priceMoment.isSameOrBefore(firstPriceMoment)
  }

  /**
   * Returns all saved price data on coin
   * @param coin
   * @returns {Promise<docs>}
   */
  async getList (coin) {
    coin = this._validateSymbol(coin)
    const docs = await db[coin].cfind({}).sort({ time: 1 }).exec()
    return docs
  }

  /**
   * Returns most recently saved price data on coin
   * @param coin
   * @returns {Promise<docs>}
   */
  async getLatest (coin) {
    coin = this._validateSymbol(coin)
    const docs = await db[coin].cfindOne({}).sort({ time: -1 }).exec()
    return docs
  }

  /**
   * Returns oldest saved price data on coin
   * @param coin
   * @returns {Promise<docs>}
   */
  async getOldest (coin) {
    coin = this._validateSymbol(coin)
    const docs = await db[coin].cfindOne({}).sort({ time: 1 }).exec()
    return docs
  }

  /**
   * nedb-promise's count does not work
   **/
  async count (coin) {
    const list = await this.getList(coin)
    return list.length
  }

  removeCoin (coin, _id) {
    coin = this._validateSymbol(coin)
    return db[coin].remove({ _id })
  }
}

module.exports = new Database()