# bittionaire

```
# install dependencies
$ yarn install

# view analytics table at localhost:3000
$ yarn start

# run simluation for coin
# @coin - 3 letter coin symbol, i.e. btc
# @interval - at which simulation runs 
# (higher the number the greater the number of sims).
# default is 50 
$ yarn sim [coin] [interval = 50]

```

Simulation 

![running simulation](https://media.giphy.com/media/1ai2b1NC0WaIFITjEx/giphy.gif)