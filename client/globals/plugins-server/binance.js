const WS = 'wss://stream.binance.com:9443/ws/'
const REST = 'https://api.binance.com/api/v1/'

class BinanceApi {
  candlestick (fn, options = { from: 'btc', to: 'eth', interval: '1m' }) {
    const { from, to, interval } = options
    const path = WS + 'btcbnb@kline_1m'
    const ws = new WebSocket(path)
    ws.onopen = () => console.log('Binance connection opened for', from, '->', to, interval)
    ws.onmessage = ({ data }) => fn(this.parseWsData(data))
    ws.onerror = (error) => console.error(error)
    return () => ws.close()
  }

  parseWsData (data) {
    return {
      timestamp: data.T,
      open: data.o,
      close: data.c,
      high: data.h,
      low: data.l
    }
  }

  async prices (from = 'ETH', to = 'BTC', interval = '1m') {
    const data = await this.get('klines?symbol=' + from.toUpperCase() + to.toUpperCase() + '&interval=' + interval)
    return this.parseRestData(data)
  }

  parseRestData (data) {
    return data.map(([timestamp, open, high, low, close, volume, quote, tradeCount]) => {
      return { timestamp, open, high, low, close, volume, quote, tradeCount }
    })
  }

  get (api) {
    const url = REST + api
    return $nuxt.$axios.$post('proxy', { url })
  }
}

export default new BinanceApi()
