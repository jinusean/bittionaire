import moment from 'moment'

const validTimes = ['day', 'hour', 'minute']
const baseUrl = 'https://graphs.coinmarketcap.com/'

export default {
  async getDominance (coin, timeframe) {
    if (validTimes.indexOf(timeframe) < 0) {
      throw new Error('invalid timeframe: ' + timeframe)
    }
    let timeTo = moment()
    let timeFrom

    switch (timeframe) {
      case 'day':
        timeFrom = moment().subtract(500, 'day')
        break
      case 'hour':
        return []
      case 'minute':
        return []
    }
    // const timeFrom = moment

    const url = baseUrl + `global/dominance/${timeFrom.valueOf()}/${timeTo.valueOf()}/`
    const data = await this.$axios.$post('/proxy', { url })
    return data.bitcoin.map(([timestamp, dominance]) => {
      return dominance
    })
  }
}