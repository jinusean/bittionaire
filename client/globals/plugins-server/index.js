import Vue         from 'vue'
import Vue2Filters from 'vue2-filters'
import moment      from 'moment'
import cc          from 'cryptocompare'
import marketcap   from './coin-marketcap'
import binance     from './binance'
import trends      from './trends'

Vue.use(Vue2Filters)

install('moment', moment)
install('cc', cc)
install('marketcap', marketcap)
install('binance', binance)
install('trends', trends)

function install (name, pckg) {
  Vue[name] = Vue.prototype['$' + name] = pckg
}
