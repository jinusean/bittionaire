class Trends {
  async timeSeries (queryString) {
    const query = '?q=' + queryString
    const data = await this.$axios.$get('trends' + query)
    return data
  }
}

export default new Trends()
