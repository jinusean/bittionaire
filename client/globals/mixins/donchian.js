export default {
  methods: {
    // 0 is the most recent
    getDonchian (data, buyPeriod = 4, sellPeriod = 5) {
      this._getDonchian(data, buyPeriod, 'high')
      this._getDonchian(data, sellPeriod, 'sell')
    },
    _getDonchian (data, period = 5, type) {
      let start = 1
      let end = (start + period + 1) > data.length ? data.length : (start + period + 1)

      while (end <= data.length) {
        const partition = data.slice(start, end)
        const datum = data[start - 1]
        start++
        end++

        if (type === 'high') {
          datum.movingHigh = this._getDonchianHighs(partition)
          if (datum.close > datum.movingHigh) {
            datum.breakoutHigh = true
          }
          continue
        }

        datum.movingLow = this._getDonchianLows(partition)
        if (datum.close < datum.movingLow) {
          datum.breakoutLow = true
        }

        if (datum.breakoutLow && datum.breakoutLow === datum.breakoutHigh) {
          delete datum.breakoutHigh
          delete datum.breakoutLow
        }
      }

      return data
    },
    _getDonchianHighs (data) {
      let high = 0
      for (const d of data) {
        if (d.close > high) {
          high = d.close
        }
      }
      return high
    },
    _getDonchianLows (data) {
      let low = Number.MAX_VALUE
      for (const d of data) {
        if (d.close < low) {
          low = d.close
        }
      }
      return low
    }
  }
}