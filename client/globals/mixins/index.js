import Vue from 'vue'
import helperMethods from './helper-methods'
import donchian from './donchian'

Vue.mixin(helperMethods)
Vue.mixin(donchian)
