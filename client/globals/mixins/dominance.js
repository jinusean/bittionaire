import moment from 'moment/moment'
import axios  from '../axios'

const validTimes = ['day', 'hour', 'minute']
const baseUrl = 'https://graphs.coinmarketcap.com/'

export default {
  methods: {
    async fetchDominance (coin, timeframe, limit = 500) {
      if (validTimes.indexOf(timeframe) < 0) {
        throw new Error('invalid timeframe: ' + timeframe)
      }
      let timeTo = moment()
      let timeFrom

      switch (timeframe) {
        case 'day':
          timeFrom = moment().subtract(limit, 'day')
          break
        case 'hour':
          return []
        case 'minute':
          return []
      }
      // const timeFrom = moment

      const url = baseUrl + `global/dominance/${timeFrom.valueOf()}/${timeTo.valueOf()}/`
      const result = await axios.post('/proxy', { url })
      const coinName = this.$store.getters.getCoinName(coin)
      const data = result[coinName]

      if (!data) {
        throw new Error('No data for', coin, 'with coin name', coinName)
      }

      return data[coinName].map(([timestamp, dominance]) => {
        return { timestamp, dominance }
      })
    }
  }
}
