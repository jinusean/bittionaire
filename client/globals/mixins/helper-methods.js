
export default {
  methods: {
    capitalize (string) {
      return string.replace(/\b\w/g, l => l.toUpperCase())
    },
    formatNumber (x) {
      if (!x) {
        return ''
      }
      return x.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',')
    }
  }
}
