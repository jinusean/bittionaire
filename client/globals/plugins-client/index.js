import Vue      from 'vue'
import './charts'
import VTooltip from 'v-tooltip'

Vue.use(VTooltip)