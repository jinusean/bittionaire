import { Scatter, mixins } from 'vue-chartjs'

export default {
  extends: Scatter,
  mixins: [mixins.reactiveProp],
  props: ['chartData', 'chartOptions'],
  computed: {
    options () {
      const yAxes = this.chartData.map(({ label }) => {
        return {
          id: label,
          type: 'linear',
          position: label === 'Trends' ? 'left' : 'right'
        }
      })

      return {
        tooltips: {
          callbacks: {
            label: (item, data) => {
              return this.$moment(item.xLabel).format('lll') + ' - ' + item.yLabel
            }
          }
        },
        scales: {
          xAxes: [{
            ticks: {
              callback: (value, index, values) => {
                return this.$moment(value).format('lll')
              },
              autoSkip: false
            }
          }],
          yAxes
        },
        maintainAspectRatio: false
      }
    },
    data () {
      const colors = ['red', 'blue', 'green', 'orange']
      const datasets = this.chartData.map(({ data, label }, index) => {
        const color = colors[index]
        return {
          data,
          label,
          showLine: true,
          radius: 1,
          borderWidth: 1,
          borderColor: color,
          backgroundColor: color,
          fill: false,
          yAxisID: label
        }
      })
      return { datasets }
    }
  },
  mounted () {
    this.renderChart(this.data, this.options)
  },
  watch: {
    chartData (chartData) {
      this.renderChart(this.data, this.options)
    }
  }
}
