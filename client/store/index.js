import coinNames from '../../simulator/CryptoBroker/symbols'

export const state = () => {
  return {
    coins: [],
    indicators: [],
  }
}

export const getters = {
  checkCoin: (state) => (coin) => {
    return state.coins.indexOf(coin) > -1
  },
  symbols () {
   return Object.keys(coinNames).map(key => coinNames[key])
  },
  coinNames () {
    return Object.keys(coinNames)
  },
  getSymbol: (state, getters) => (name) => {
    return coinNames[name]
  },
  getCoinName: (state, getters) => (symbol) => {
    let coinName = ''
    Object.keys(coinNames).forEach(key => {
      const sym = coinNames[key]
      if (sym === symbol) {
        coinName = key
      }
    })
    return coinName
  }
}

export const mutations = {
  setCoins (state, coins) {
    state.coins = coins
  },
  setIndicators (state, indicators) {
    state.indicators = indicators
  }
}

export const actions = {
  async nuxtServerInit ({ commit }, { req }) {
    await Promise.all([
      this.$axios.$get('coins').then(coins => commit('setCoins', coins)),
      this.$axios.$get('indicators').then(indicators => commit('setIndicators', indicators))
    ])
  }
}
