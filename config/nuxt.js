require('dotenv').config({})

module.exports = {
  dev: true,
  head: {
    title: 'Bittionaire',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'Nuxt.js project' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },
  /*
  ** Global CSS
  */
  css: ['milligram/dist/milligram.css', 'styles/main.scss', 'styles/v-tooltip.scss'],
  /*
  ** Add axios globally
  */
  modules: ['@nuxtjs/axios'],
  axios: {
    baseURL: `http://localhost:${process.env.API_PORT}/api`
  },
  plugins: [
    { src: 'globals/vue-config', ssr: true },
    { src: 'globals/plugins-client', ssr: false },
    { src: 'globals/plugins-server', ssr: true },
    { src: 'globals/mixins', ssr: true },
    { src: 'globals/filters', ssr: true }
  ],
  srcDir: 'client',
  build: {
    vendor: [ 'moment'],
    babel: {
      plugins: [
        'transform-object-rest-spread',
        'transform-object-assign'
      ]
    },
    extend (config) {
      config.resolve.alias.simulator = '~/../simulator'
      config.resolve.alias.indicators = '~/../simulator/indicators'
    }
  }
}
