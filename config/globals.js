const axios = require('axios')
const instance = axios.create()

instance.interceptors.response.use(function (response) {
  return response ? response.data : response
}, function (error) {
  return Promise.reject(error)
})

global.axios = instance
global.fetch = require('node-fetch')
global.moment = require('moment')
global._ = require('lodash')
