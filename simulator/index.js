const CryptoBroker = require('./CryptoBroker')
const coins = require('../config/coins')
const runDonchian = require('./runDonchian')

const Simulator = {
  async start (coin, interval) {
    if (!coin) {
      throw new Error('Coin is required')
    }
    coin = coin.toUpperCase()

    await this.updateCoin(coin)
    await this.updateObv(coin)
    await this.runDonchian(coin, interval)
  },

  async saveDominanceData () {
    console.log('[Simulator] Starting to extract dominance data....')
    await CryptoBroker.saveDominanceData()
    console.log('[Simulator] Finished extracting dominance data')
    console.log('------------------------------------------------')
  },

  async updateCoin (coin) {
    console.log('Updating prices for', coin)
    try {
      await CryptoBroker.updatePrices(coin, true)
      console.log('Finished updating prices for coin', coin)
      console.log('----------------------------------------')
    } catch (error) {
      console.log(error)
    }
  },

  async updateObv (coin) {
    console.log('Updating OBV for ', coin)
    await CryptoBroker.updateObv(coin)
    console.log('Finished updating OBV for coin', coin)
    console.log('----------------------------------------')
  },
  runDonchian
}

module.exports = Simulator
