/**
 * Returns Array of On-Balance-Volume values
 * @param dataset
 * @returns {Array}
 */
module.exports = function (dataset, obv = 0) {
  console.log(dataset)
  if (!dataset) throw new Error('dataset is required')

  if (dataset.length < 2) {
    return dataset
  }

  const copy = dataset.map(d => d)

  // If index 0 is the earliest
  if (copy[0].time > copy[1].time) {
    copy.reverse()
  }

  copy[0].obv = obv

  for (let i = 1; i < copy.length; i++) {
    const yesterday = copy[i - 1]
    const today = copy[i]

    if (today.close > yesterday.close) {
      obv += today.volumeto
    } else if (today.close < yesterday.close) {
      obv -= today.volumeto
    }
    today.obv = obv
  }

  return dataset
}
