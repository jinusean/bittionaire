/**
 * Returns partitioned data
 * @param data {Array}
 * @param period {Number}
 * @returns {Array} = {
 *           prices:    {Coin}   relative period's historical coin price values,
 *           period:  {Number}  period number,
 *           data:    {Array}   period's data
 */
module.exports = function partition (data, period) {
  if (!data) {
    throw new Error('Data must be defined')
  }
  if (!period) {
    period = data.length > 20 ? 20 : data.length - 1
  }
  if (data.length < period) {
    throw new Error(`Data (${data.length}) is less than specified periods (${period})`)
  }

  const result = []

  for (let start = 0, i = period; i < data.length - 1; i++, start++) {
    const periodData = { prices: data[i + 1], period: start + 1, data: data.slice(start, i) }
    result.push(periodData)
  }

  return result
}
