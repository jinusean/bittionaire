function donchian (dataset, period) {
  const result = []
  const uptrends = []
  const downtrends = []

  for (let start = 0, i = period; i < dataset.length - 1; i++, start++) {
    const prices = dataset[i + 1]
    if (!prices) {
      continue
    }
    const data = dataset.slice(start, i)
    const currentPeriod = start + 1
    let highest = null
    let lowest = null

    data.forEach(datum => {
      const { high, low } = datum
      if (highest === null) {
        highest = high
        lowest = low
      }
      if (high > highest) {
        highest = high
      }
      if (low < lowest) {
        lowest = low
      }
    })

    const trendData = Object.assign({}, prices, {
      movingHigh: highest,
      movingLow: lowest,
      period: currentPeriod
    })
    result.push(trendData)

    // Ignore concurrent signals
    if (prices.high > highest && prices.low < lowest) {
      continue
    }

    if (prices.high > highest) {
      uptrends.push(trendData)
    }
    if (prices.low < lowest) {
      downtrends.push(trendData)
    }
  }

  return { uptrends, data: result, downtrends }
}

module.exports = donchian
// export default donchian
