const database = require('../../database')
const Obv = require('../indicators/obv')

module.exports = {
  async printObv (coin) {
    const obvs = await this.fetchObv(coin)
    obvs.reverse().forEach(data => {
      const { timestamp, obv, close } = data
      console.log(moment(timestamp).format('LL') + '      OBV:', obv, '              Close:', close)
    })
  },

  async fetchObv (coin) {
    const prices = await database.getList(coin)
    return Obv(prices)
  },

  getObv (dataset) {
    return Obv(dataset)
  },

  async updateObv (coin) {
    const OBV = 'obv'
    const lastObv = await database.getIndicator(OBV, coin)
    if (lastObv && moment().isSame(moment(lastObv.timestamp), 'days')) {
      return console.log('No need to update - obv:', lastObv.obv)
    }
    const obvs = await this.fetchObv(coin)
    const result = obvs[obvs.length - 1]

    await database.saveObv(coin, result)
  }
}
