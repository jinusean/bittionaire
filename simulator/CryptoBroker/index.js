const cc = require('cryptocompare')
const database = require('../../database')
const moment = require('moment')
const updatePrices = require('./updatePrices')
const obv = require('./obv')
const donchian = require('./donchian')
const symbols = require('./symbols')
const dominanceMethods = require('./dominance-methods')
const priceMethods = require('./price-methods')

const VALID_TIMES = ['day', 'hour', 'minute']

const CryptoBroker = {
  updatePrices,
  ...donchian,
  ...obv,
  ...dominanceMethods,
  ...priceMethods,

  _validateTime (time) {
    time = time.toLowerCase()
    for (const validTime of VALID_TIMES) {
      if (validTime === time) {
        return validTime
      }
      if (validTime + 's' === time) {
        return validTime
      }
    }
    throw new Error('Invalid timeframe: ' + time)
  },

  async _downloadData (coin, options) {
    const docs = await cc.histoDay(coin, 'USD', options)
    const promises = docs.map(async doc => {
      await database.save(coin, doc)
      console.log('Saved ', coin, moment(doc.timestamp).format('LL'), doc.close)
    })
    return Promise.all(promises)
  },

  getSymbol (coinname) {
    return symbols[coinname]
  },

  getCoinName (symbol) {
    return _.invert(symbols)[symbol]
  },

  async fetchAnalyticsData ({ coin, time, limit = 500, high = 4, low = 5, start = 100 }) {
    const [pricesData, dominanceData] = await Promise.all([
      this.fetchPrices(coin, time, {}).catch(e => {
        console.error('PricesData ' + e)
        return []
      }),
      this.fetchDominance(coin, time, {}).catch(e => {
        console.error('DominanceData ' + e)
        return []
      })    // Don't bother using limit here
    ])
    let data = []

    if (dominanceData.length > 0 && pricesData.length > 0) {
      let shorterLength = (dominanceData.length < pricesData.length ? dominanceData.length : pricesData.length)
      shorterLength = shorterLength < limit ? shorterLength : limit

      pricesData.reverse()
      dominanceData.reverse()

      for (let i = 0; i < shorterLength; i++) {
        const price = pricesData[i]
        const dominance = dominanceData[i]

        const priceTime = moment(price.time * 1000)
        const dominanceTime = moment(dominance.timestamp)

        if (priceTime.diff(dominanceTime, 'days') > 1) {
          throw new Error('Time difference greater than 1 day - Price: ' + priceTime.format('lll') + ', Dominance: ' + dominanceTime.format('lll'))
        }

        const mergedData = Object.assign({}, price, dominance)
        data.push(mergedData)
      }
    } else if (dominanceData.length === 0 && dominanceData.length === 0) {
      data = []
    } else if (pricesData.length === 0) {
      data = dominanceData
    } else if (dominanceData.length === 0) {
      data = pricesData
    }

    data = this.getObv(data)
    data = this.getDonchian(data, high, low)
    data = this.runDonchian(data)
    return data
  }
}

module.exports = CryptoBroker
