const cc = require('cryptocompare')

module.exports = {
  async fetchPrices (from, time, { to = 'USD', limit = 500 }) {
    from = from.toUpperCase()
    time = this._validateTime(time)

    const historyData = await cc['histo' + _.capitalize(time)](from, to.toUpperCase(), { limit })
    return historyData
  }
}
