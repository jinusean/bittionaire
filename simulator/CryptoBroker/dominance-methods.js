const BASE_URL = 'https://graphs2.coinmarketcap.com/'

module.exports = {
  async fetchDominance (coinSymbol, time, { limit = 500 }) {
    time = this._validateTime(time)
    let timeTo = moment()
    let timeFrom

    switch (time) {
      case 'day':
        timeFrom = moment().subtract(limit, 'day')
        break
      case 'hour':
      case 'minute':
        throw new Error('Time ' + time + ' not yet implemented')
    }

    const url = BASE_URL + `global/dominance/${timeFrom.valueOf()}/${timeTo.valueOf()}/`
    const result = await axios.get(url)

    const coinName = this.getCoinName(coinSymbol.toLowerCase())

    if (!coinName) {
      return throwError(coinSymbol)
    }

    const data = result[coinName]

    if (!data) {
      return throwError(coinSymbol)
    }

    return data.map(([timestamp, dominance]) => {
      return { timestamp, dominance }
    })
  }
}

function throwError (coinSymbol) {
  throw new Error('Dominance data for coin with symbol ' + coinSymbol + ' not available')
}
