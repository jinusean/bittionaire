const database = require('../../database')

/**
 * Downloads new data if most recent price downloaded is before today
 * @param coin
 * @param untilFirstPrice {boolean} - if true continues downloading data until first price is found
 * @returns {Promise<void>}
 */
module.exports = async function (coin, untilFirstPrice = false) {
  if (!coin) {
    throw new Error('Coin required')
  }

  const options = {}

  const existingDocs = await database.getList(coin)

  if (existingDocs.length === 0) {
    console.log('Starting fresh download of ', coin)
    options.limit = 'none'
    return this._downloadData(coin, options)
  }

  const todaysMoment = moment()
  const mostRecentDoc = existingDocs[existingDocs.length - 1]
  const mostRecentTime = mostRecentDoc.timestamp
  const mostRecentMoment = moment(mostRecentTime)
  const mostRecentDate = new Date(mostRecentTime)
  const diffDays = todaysMoment.diff(mostRecentMoment, 'days')

  if (diffDays === 0) {
    return
  }

  options.limit = diffDays || 1
  options.timestamp = mostRecentDate

  await this._downloadData(coin, options)

  if (diffDays <= 2000) {
    return
  }

  return this.updatePrices(coin)
}
