module.exports = {
  runDonchian (data, amount = 100) {
    if (data.length < 2) {
      return data
    }

    let lastPosition = 'sell'
    let coins = 0

    const copy = data.map(d => d)

    // If index 0 is the most recent
    if (copy[0].time > copy[1].time) {
      copy.reverse()
    }

    // Now Index 0 is the earliest
    for (const d of copy) {
      if (lastPosition === 'sell' && d.breakoutHigh) {
        coins = amount / d.close
        amount = 0
        lastPosition = 'buy'
        d.donchian = 'Bought: ' + coins.toFixed(5) + ' coins'
      }

      if (lastPosition === 'buy' && d.breakoutLow) {
        amount = coins * d.close
        coins = 0
        lastPosition = 'sell'
        d.donchian = 'Sold: $' + amount.toFixed(2)
      }
    }

    const finalData = copy[data.length - 1]
    if (lastPosition === 'buy') {
      amount = coins * finalData.close
    }

    finalData.donchian = amount.toFixed(2)

    return data
  },

  // Index 0 === earliest
  getDonchian (data, buyPeriod = 4, sellPeriod = 5) {
    data = this._getDonchian(data, buyPeriod, 'high')
    data = this._getDonchian(data, sellPeriod, 'sell')
    return data
  },
  _getDonchian (data, period = 5, type) {
    if (data.length < period) {
      return data
    }

    let start = 1
    let end = (start + period + 1) > data.length ? data.length : (start + period + 1)
    const result = []

    while (end <= data.length) {
      const partition = data.slice(start, end)
      const datum = { ...data[start - 1] }
      start++
      end++

      if (type === 'high') {
        datum.movingHigh = this._getDonchianHighs(partition)

        if (datum.close > datum.movingHigh) {
          datum.breakoutHigh = true
        }
      }

      if (type === 'sell') {
        datum.movingLow = this._getDonchianLows(partition)

        if (datum.close < datum.movingLow) {
          datum.breakoutLow = true
        }
      }

      if (datum.breakoutLow && datum.breakoutLow === datum.breakoutHigh) {
        delete datum.breakoutHigh
        delete datum.breakoutLow
      }

      result.push(datum)
    }

    while (start < data.length) {
      const datum = { ...data[start - 1] }
      result.push(datum)
      start++
    }

    return result
  },
  _getDonchianHighs (data) {
    let high = 0
    for (const d of data) {
      if (d.close > high) {
        high = d.close
      }
    }
    return high
  },
  _getDonchianLows (data) {
    let low = Number.MAX_VALUE
    for (const d of data) {
      if (d.close < low) {
        low = d.close
      }
    }
    return low
  }
}
