const database = require('../database')
const Pool = require('threads').Pool
const donchian = require('./indicators/donchian')

module.exports = async function (coin, interval = 50) {
  const startingAmount = 100
  const minPeriod = 1

  const coinPrices = await database.getList(coin)
  const lastCoinPrice = coinPrices[coinPrices.length - 1]
  const maxPeriod = await database.count(coin)
  let bestAmount = 0
  let bestBuyPeriod = 0
  let bestSellPeriod = 0

  const totalPeriods = Math.floor(coinPrices.length / interval)
  const firstQuarter = interval * Math.floor(totalPeriods * 0.25)
  const secondQuarter = interval * Math.floor(totalPeriods * 0.50)
  const thirdQuarter = interval * Math.floor(totalPeriods * 0.75)

  const pool = new Pool().run(runDonchianTask)
  const promises = []

  for (let buyPeriod = minPeriod; buyPeriod <= maxPeriod; buyPeriod += interval) {
    console.log('Starting for buy period:', buyPeriod)
    const { uptrends } = donchian(coinPrices, buyPeriod)
    const promise = pool
      .send({
        uptrends,
        coinPrices,
        buyPeriod,
        minPeriod,
        lastCoinPrice,
        maxPeriod,
        startingAmount,
        interval,
        __dirname
      })
      .on('done', function ({ buyPeriod, sellPeriod, roi, gains, amount }) {
        let message = `Finished: Buy: ${buyPeriod}       Sell: ${sellPeriod}    ROI: ${roi}     Net: ${gains}     Total: ${amount}`
        // Set as best periods
        if (amount > bestAmount) {
          bestAmount = amount
          bestBuyPeriod = buyPeriod
          bestSellPeriod = sellPeriod
          message += '     NEW BEST'
        }

        console.log(message)

        switch (buyPeriod - 1) {
          case firstQuarter:
            return console.log('   Progress: 1st Quarter')
          case secondQuarter:
            return console.log('   Progress: 2nd Quarter')
          case thirdQuarter:
            return console.log('   Progress: 3rd Quarter')
        }
      })
      .on('error', function (e) {
        console.error(e)
      }).promise()
    promises.push(promise)
  }

  await Promise.all(promises)
  console.log(`\nFinished with best amount of $${bestAmount}`)
  console.log('Buy period:', bestBuyPeriod)
  console.log('Sell period:', bestSellPeriod)
  console.log('----------------------------------------------------------------------------')
  pool.killAll()
}

function runDonchianTask ({ uptrends, coinPrices, buyPeriod, minPeriod, lastCoinPrice, maxPeriod, startingAmount, interval, __dirname }, done) {
  const resolve = require('path').resolve
  const donchian = require(resolve(__dirname, 'indicators/donchian'))
  let bestSellPeriod = minPeriod
  let bestAmount = 0

  for (let sellPeriod = minPeriod; sellPeriod <= maxPeriod; sellPeriod += interval) {
    const { downtrends } = donchian(coinPrices, sellPeriod)
    let currentAmount = startingAmount
    let currentCoins = 0
    let lastSignal = 'sell' // Start at sell position in order to make first trade a buy
    let sellIndex = 0
    let buyIndex = 0
    let buySignalCount = 0
    let sellSignalCount = 0

    while (sellIndex < downtrends.length && buyIndex < uptrends.length) {
      const buyPrice = uptrends[buyIndex]
      const sellPrice = downtrends[sellIndex]

      // Ignore indicator
      if (buyPrice.time === sellPrice.time) {
        // console.log('buy', buyPrice)
        // console.log('--------------')
        // console.log('sell', sellPrice)
        // console.log('--------------')
        // throw new Error('Buy signal and sell signal on the same date ')
        return
      }

      // Handle uptrend
      if (buyPrice.time < sellPrice.time) {
        buyIndex++
        if (lastSignal === 'buy') {
          continue
        }

        lastSignal = 'buy'
        buySignalCount++
        currentCoins = currentAmount / buyPrice.close
        currentAmount = 0
        continue
      }

      // Handle downtrend
      sellIndex++
      if (lastSignal === 'sell') {
        continue
      }

      lastSignal = 'sell'
      sellSignalCount++
      currentAmount = sellPrice.close * currentCoins
      currentCoins = 0
    }

    // Final signal is a buy-signal
    if (buyIndex < uptrends.length && lastSignal !== 'buy') {
      const buyPrice = uptrends[buyIndex]
      currentCoins = currentAmount / buyPrice.close
      currentAmount = 0
      buySignalCount++
    } else if (sellIndex < downtrends.length && lastSignal !== 'sell') {
      const sellPrice = downtrends[sellIndex]
      currentAmount = sellPrice.close * currentCoins
      currentCoins = 0
      sellSignalCount++
    }

    // Swap coins
    if (currentAmount === 0) {
      currentAmount = currentCoins * lastCoinPrice.close
    }

    if (currentAmount > bestAmount) {
      bestSellPeriod = sellPeriod
      bestAmount = currentAmount
    }
  }

  bestAmount = bestAmount.toFixed(2)
  const roi = ((bestAmount - startingAmount) / startingAmount).toFixed(2)
  const gains = (bestAmount - startingAmount).toFixed(2)
  done({ buyPeriod, sellPeriod: bestSellPeriod, roi, gains, amount: bestAmount })
}
