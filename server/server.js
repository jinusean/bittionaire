const express = require('express')
const bodyParser = require('body-parser')
const morgan = require('morgan')
const cors = require('cors')

// Render every route with nuxt.js
const app = express()

app.use(cors())
app.use(bodyParser.json())
app.use(morgan('dev'))

app.use('/api', require('./routes'))

const port = process.env.API_PORT
app.listen(port, () => console.log(`Server listening on localhost:${port}`))
