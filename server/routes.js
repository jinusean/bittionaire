const Router = require('express').Router
const coins = require('../config/coins')
const indicators = require('../config/indicators')
const CryptoBroker = require('../simulator/CryptoBroker')
const axios = require('axios')
const googleTrends = require('google-trends-api')

const router = Router()

router.get('/trends', async function (req, res, next) {
  const keyword = req.query.q
  const startTime = moment().subtract(1, 'hour').toDate()
  const granularTimeResolution = true

  try {
    const data = await googleTrends.interestOverTime({ keyword, startTime, granularTimeResolution })
    res.json(JSON.parse(data).default.timelineData.map(({ time, value }) => {
      return { timestamp: time * 1000, value: value[0] }
    }))
  } catch (error) {
    console.log('There\'s a good chance the rate limit exceeded. Try back in an hour')
    next(error)
  }
})

router.post('/proxy', async function (req, res, next) {
  const url = req.body.url
  if (!url) {
    return res.badRequestResponse('No url specified')
  }
  try {
    const { data } = await axios.get(url)
    res.json(data)
  } catch (error) {
    next(error)
  }
})

router.get('/coins', async function (req, res, next) {
  res.json(coins)
})

router.get('/indicators', async function (req, res, next) {
  res.json(indicators)
})

router.get(`/:coin/obv`, async function (req, res, next) {
  const coin = req.params.coin.toLowerCase()
  const result = await CryptoBroker.getObv(coin)
  res.json(result)
})

router.get(`/:coin/dominance`, async function (req, res) {
  const coin = req.params.coin.toLowerCase()
  const Datastore = require('nedb-promise')
  const path = require('path')
  const database = await Datastore({
    filename: path.resolve(__dirname, '..', 'database', 'dominance', coin + '.txt'),
    autoload: true
  })
  const data = await database.cfind({}).sort({ timestamp: 1 }).exec()
  res.json(data)
})

router.get(`/:coin`, async function (req, res, next) {
  const coin = req.params.coin
  const time = req.query.time || 'day'
  const limit = parseInt(req.query.limit || 500)
  const high = parseInt(req.query.high || 5)
  const low = parseInt(req.query.low || 4)

  const data = await CryptoBroker.fetchAnalyticsData({ coin, time, limit, high, low })
  res.json(data)
})

module.exports = router
