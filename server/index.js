require('dotenv').config({})
process.on('unhandledRejection', r => console.error(r))

require('module-alias/register')
require('../config/globals')
require('./server')
